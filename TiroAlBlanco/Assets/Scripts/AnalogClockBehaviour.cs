﻿using UnityEngine;
using System.Collections;

//! AnalogClockBehaviour class
/*! This class explains the behaviour of the GameObject "Clock" and all its children objects
 * The clock and the game are set to countdown up to 3 minutes and then finish the application
 */
public class AnalogClockBehaviour : MonoBehaviour {

	GameObject clockBody; /*!< This variable represents the GameObject "Clock" i the scene*/
	static float timeToDegrees; /*!< This float variable is the division of the starting time    and the 360 degrees */
	float blinkTime; /*!< This variable represents the actual time of blinking of the clock after the time threshold is reached */
	float maxBlinkTime; /*!< This variable is the maximum time the clock will blink after the time threshold is reached */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		clockBody = GameObject.Find("Clock");
		timeToDegrees = 360f/Camera.main.GetComponent<CameraBehaviour>().GetStartingTimeLeft();
		blinkTime = 0.0f;
		maxBlinkTime = 0.5f;
	}

	//! Update() function
	/*! This function is called every frame it refreshes the
	 * movement of the clock, lifetime and the destruction of the GameObject "clock"
	 */
	void Update () {
		this.transform.localEulerAngles = new Vector3(0,0,(Camera.main.GetComponent<CameraBehaviour>().GetTimeLeft())* timeToDegrees);
		if(Camera.main.GetComponent<CameraBehaviour>().GetTimeLeft() <= 10.0f){
			blinkTime += Time.deltaTime;
			if(blinkTime > maxBlinkTime){
				blinkTime = 0.0f;
				if(clockBody.GetComponent<SpriteRenderer>().color == Color.red)
					clockBody.GetComponent<SpriteRenderer>().color = Color.white;
				else
					clockBody.GetComponent<SpriteRenderer>().color = Color.red;
			}

		}
		if(Camera.main.GetComponent<CameraBehaviour>().GetTimeLeft() <= 0.0f)
			Destroy(GameObject.Find("Clock"));
	}
}
