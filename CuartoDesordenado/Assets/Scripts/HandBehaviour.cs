﻿using UnityEngine;
using System.Collections;

//! HandBehaviour() class
/*! This class handles the behaviour of the GameObject avatar "hand" which plays the main interaction with the user.
 * The animation of this GameObject, reading from the different inputs from the "Gripper" 
 * and some GUI elements are managed in this script.
 */
public class HandBehaviour : MonoBehaviour {

	Animator anim; /*!< this variable represents the animator of the "hand" GameObject */
	GameObject grabbedObject; /*!< This GameObject is the "object" that has being grabbed an it's following the position of the "hand" */
	bool onObject; /*!< this flag is raised whe the "hand" is over an "object" */
	public GUIStyle instructionStyle; /*!< This variable represents the text style of the intructions in the GUI. This can be changed in the Unity Editor */
	public Texture moveGripper; /*!< This texure variable represents the move action instruction */
	public Texture grabGripper; /*!< This texure variable represents the grab action instruction */
	public Texture releaseGripper; /*!< This texure variable represents the realease action instruction */
	bool hold; /*!< This flag is set using the "PDController"  */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	*/
	void Start () {
		anim = this.GetComponent<Animator>();
		onObject = false;
		hold = false;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * position of the avatar, pressure sensing, slot instantiation and destruction, grab and release of "objects"
	 * and GUI flag controller and other GUI components like the instructions textures
	 */
	void Update () {
		if(transform.position.x >= 7.5) transform.position = new Vector3(7.49f,transform.position.y,transform.position.z);
		if(transform.position.x <= -7.5) transform.position = new Vector3(-7.49f,transform.position.y,transform.position.z);
		if(transform.position.y >= 5.5) transform.position = new Vector3(transform.position.x,5.49f,transform.position.z);
		if(transform.position.y <= -5.5) transform.position = new Vector3(transform.position.x,-5.49f,transform.position.z);
		if((transform.position.x < 7.5 && transform.position.x > -7.5) && (transform.position.y < 5.5 && transform.position.y > -5.5)){
			if(!grabbedObject){
				transform.position = new Vector3(7.5f*Input.GetAxis("Horizontal"),5.5f*Input.GetAxis("Vertical"));
			}else{
				if(grabbedObject.GetComponent<ObjectBehaviour>().ThisSlotExists()){
					if((new Bounds(grabbedObject.GetComponent<ObjectBehaviour>().GetThisSlot().GetComponent<BoxCollider2D>().bounds.center,new Vector3(3.0f*grabbedObject.GetComponent<ObjectBehaviour>().GetThisSlot().GetComponent<BoxCollider2D>().bounds.size.y,3.0f*grabbedObject.GetComponent<ObjectBehaviour>().GetThisSlot().GetComponent<BoxCollider2D>().bounds.size.y,1))).Contains(new Vector3(7.5f*Input.GetAxis("Horizontal"),5.5f*Input.GetAxis("Vertical")))){
						transform.position = grabbedObject.GetComponent<ObjectBehaviour>().GetThisSlot().transform.position - 2*(Vector3)this.GetComponent<BoxCollider2D>().center;
					}else{
						transform.position = new Vector3(7.5f*Input.GetAxis("Horizontal"),5.5f*Input.GetAxis("Vertical"));
					}
				}
			}
		}
		anim.SetFloat("pressure",Input.GetAxis("Pressure"));
		if(this.gameObject.GetComponent<PDController>().Hold()){
			hold = true;
			if(grabbedObject){
				if(grabbedObject.GetComponent<ObjectBehaviour>()){
					grabbedObject.GetComponent<ObjectBehaviour>().GrabObject();
				}
			}
		}else{
			hold = false;
			if(grabbedObject){
				if(grabbedObject.GetComponent<ObjectBehaviour>()){
					grabbedObject.GetComponent<ObjectBehaviour>().ReleaseObject();
					grabbedObject = null;
				}
			}
		}
	}

	//! Public function IsHoldingAnOnject()
	/*! This function returns true/false if the avatar has a grabbed "object" 
	 */
	public bool IsHoldingAnObject(){
		return (bool)grabbedObject;
	}

	//! OnTriggerEnter2D() function
	/*! This function is called when entering a trigger collider. If the collider is a component of an "object"
	 * then it changes its color to yellow
	 */
	void OnTriggerEnter2D(Collider2D other){
		if(other.tag == "Objects"){
			if(other.GetComponent<ObjectBehaviour>()){
				if(other.GetComponent<ObjectBehaviour>().GetHoldable() && !(bool)grabbedObject){
					other.GetComponent<SpriteRenderer>().color = Color.yellow;
					onObject = true;
				}
			}
		}
	}

	//! OnTriggerStay2D() function
	/*! This function is called while on a trigger collider. If the collider is a component of an "object"
	 * then it changes its color to yellow
	 */
	void OnTriggerStay2D(Collider2D other){
		if(other.tag == "Objects"){
			if(other.GetComponent<ObjectBehaviour>()){
				if(other.GetComponent<ObjectBehaviour>().GetHoldable() && !(bool)grabbedObject){
					onObject = true;
					other.GetComponent<SpriteRenderer>().color = Color.yellow;
				} else{
					other.GetComponent<SpriteRenderer>().color = Color.white;
				}
				if(hold){
					if(!grabbedObject && other.GetComponent<ObjectBehaviour>().GetHoldable()){
						grabbedObject = other.gameObject;
					}
				}
			}
		}
	}

	//! OnTriggerExit2D() function
	/*! This function is called when exiting a trigger collider. If the collider is a component of an "object"
	 * then it changes its color to white
	 */
	void OnTriggerExit2D(Collider2D other){
		if(other.tag == "Objects"){
			if(other.GetComponent<ObjectBehaviour>()){
				other.GetComponent<SpriteRenderer>().color = Color.white;
				onObject = false;
			}
		}
	}

	//!OnGUI() function
	/*! This script contains some GUI elements and feedback of the game, these are instructions with 2D textures. 
	 * Most of this GUI layer is planned for a resolution of 1024*768
	 */
	void OnGUI(){
		if(grabbedObject){
			GUI.TextArea(new Rect(70,30,300,50),"Release the Object",instructionStyle);
			GUI.DrawTexture(new Rect(10,10,0.75f*releaseGripper.width,0.75f*releaseGripper.height),releaseGripper);
		} else{
			if(!onObject){
				GUI.TextArea(new Rect(70,30,300,50),"Select an Object",instructionStyle);
				GUI.DrawTexture(new Rect(10,10,0.75f*moveGripper.width,0.75f*moveGripper.height),moveGripper);
			}else{
				GUI.TextArea(new Rect(70,30,300,50),"Grab the Object",instructionStyle);
				GUI.DrawTexture(new Rect(10,10,0.75f*grabGripper.width,0.75f*grabGripper.height),grabGripper);
			}
		}
	}

}
