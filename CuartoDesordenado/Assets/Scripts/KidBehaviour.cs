﻿using UnityEngine;
using System.Collections;

//! KidBehaviour class
/*! This class describes the behaviour of the "kid" GameObject, which main objective is to keep the "objects"
 *  out of their respective goal positions. If the room is still disordered after some time (40 seconds)
 *  the "kid" will come back and take some "objects" out of place. If the room is in order, the "kid" will 
 *  come back immediately to disorder some of the "objects" 
 */
public class KidBehaviour : MonoBehaviour {

	Animator anim; /*!< This variable represents the animator controller for the "kid" GameObject */
	public GameObject dust; /*!< This prefab GameObject is part of the particle system shown when the kid is messing the room */
	public GameObject sparkle; /*!< This prefab GameObject is part of the particle system shown when the room is completely in order */
	public AudioClip happyClip; /*!< This audio clip is played the "kid" messes some "objects" but the room is clean */
	public AudioClip angryClip; /*!< This audio clip is played the "kid" messes some "objects" are still away from their goal position */
	public AudioClip cleanClip; /*!< This audio clip is played when the user puts every "object" in their goal position */
	bool start; /*!< This flag is only raised at the beginning of the game and disables the clean animation */
	bool firstScramble; /*!< This flag is only raised at the beginning of the game and starts the disorder animation */
	bool spam; /*!< This flag is raised everytime the "kid" is messing room */
	bool returning; /*!< This flag is raised when the kid timer is over and the "kid" returns */
	bool playable; /*!< This flag is raised when there's no audio clip playing currently */
	float spammingTime; /*!< This variable represents the time of instantiation of the "dust" particles */
	float returnTime; /*!< This variable represents the current time of return of the "kid" */
	float returnTimeLimit; /*!< This variable represents the maximum return time of the "kid" */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	*/
	void Start () {
		anim = this.GetComponent<Animator>();
		anim.SetBool("watch",true);
		start = true;
		firstScramble = true;
		spam = false;
		returning = false;
		playable = true;
		spammingTime = 0.0f;
		returnTime = 0.0f;
		returnTimeLimit = 40.0f;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * time countdown, timers, particle system instantiation, position of "objects" check
	 * and GUI flag controller like the kid texture bar
	 */
	void Update () {
		if(spam){
			spammingTime += Time.deltaTime;
			if (spammingTime > 0.3f){
				Instantiate(dust,new Vector3(this.transform.position.x+4*Random.insideUnitCircle.x,4*Random.insideUnitCircle.y,0),Quaternion.identity);
				Instantiate(dust,new Vector3(this.transform.position.x+4*Random.insideUnitCircle.x,4*Random.insideUnitCircle.y,0),Quaternion.identity);
				Instantiate(dust,new Vector3(this.transform.position.x+4*Random.insideUnitCircle.x,4*Random.insideUnitCircle.y,0),Quaternion.identity);
				Instantiate(dust,new Vector3(this.transform.position.x+4*Random.insideUnitCircle.x,4*Random.insideUnitCircle.y,0),Quaternion.identity);
				Instantiate(dust,new Vector3(this.transform.position.x+4*Random.insideUnitCircle.x,4*Random.insideUnitCircle.y,0),Quaternion.identity);
				Instantiate(dust,new Vector3(this.transform.position.x+4*Random.insideUnitCircle.x,4*Random.insideUnitCircle.y,0),Quaternion.identity);
				Instantiate(dust,new Vector3(this.transform.position.x+4*Random.insideUnitCircle.x,4*Random.insideUnitCircle.y,0),Quaternion.identity);
				spammingTime = 0.0f;
			}
		}
		if(returning){
			returnTime += Time.deltaTime;
			if(returnTime >= returnTimeLimit || OrderCheck()){
				anim.SetBool("return",true);
				returning = false;
				returnTime = 0.0f;
			}
		}
		if(OrderCheck()){
			anim.SetBool("suspend",true);
			Instantiate(sparkle,new Vector3(Random.Range(-7.5f,7.5f),Random.Range(-5.5f,5.5f),0),Quaternion.identity);
			Camera.main.audio.Stop();
			if(playable){
				ChangeDifficulty();
				this.audio.Stop();
				this.audio.Play();
				this.audio.PlayOneShot(cleanClip);
				playable = false;
			}
		}else{
			playable = true;
		}
	}


	//! StopSuspend() function
	/*! This function sets the animation variable "suspend" in the animation controller to false in order to
	 * resume the current animation
	 */
	void StopSuspend(){
		anim.SetBool("suspend",false);
	}

	//! StartWatching() function
	/*! This function sets the animation variable "watch" in the animation controller to true in order to
	 * check the position of each "object" in the room
	 */
	void StartWatching(){
		anim.SetBool("watch",true);
	}

	//! StopWatching() function
	/*! This function sets the animation variable "watch" in the animation controller to false in order to
	 *  stop checking the position of each "object" in the room
	 */
	void StopWatching(){
		anim.SetBool("watch",false);
	}

	//! CheckIfItsInOrder() function
	/*! This function sets the animation variable "isInOrder" in the animation controller to true or false
	 * depending on the position of every "object" in the room. This function is called in the animation clips in
	 * the Unity Editor
	 */
	void CheckIfItsInOrder(){
		if(start){
			anim.SetBool("isInOrder",false);
		}else{
			bool isInOrder = true;
			GameObject[] gameobjects = GameObject.FindGameObjectsWithTag("Objects");
			foreach(GameObject gameobject in gameobjects){
				if(gameobject.GetComponent<ObjectBehaviour>()){
					isInOrder = gameobject.GetComponent<ObjectBehaviour>().IsInOrder();
					if(!isInOrder)
						break;
				}
			}
			anim.SetBool("isInOrder",isInOrder);
		}
	}

	//! OrderCheck() function
	/*! This function is called in order to refresh the status of all the "objects" in the room
	 * it returns false if at least one objects is out of its goal position and true otherwise
	 */
	bool OrderCheck(){
		if(!start){
			bool isInOrder = true;
			GameObject[] gameobjects = GameObject.FindGameObjectsWithTag("Objects");
			foreach(GameObject gameobject in gameobjects){
				if(gameobject.GetComponent<ObjectBehaviour>()){
					isInOrder = gameobject.GetComponent<ObjectBehaviour>().IsInOrder();
					if(!isInOrder)
						break;
				}
			}
			return isInOrder;
		}else{
			return false;
		}
	}

	//! ChangeDifficulty() function
	/*! This function is called in order to increase or repeat the difficulty of the game with the current scene.
	 * This version of the game has currently 3 levels of difficulty based on the distance between the current position 
	 * of the "objects" and the goal position of each one. After the last difficulty is reached and successfully achieved, 
	 * the difficulty is reset
	 */
	void ChangeDifficulty(){
		GameObject[] gameobjects = GameObject.FindGameObjectsWithTag("Objects");
		foreach(GameObject gameobject in gameobjects){
			if(gameobject.GetComponent<ObjectBehaviour>()){
				gameobject.GetComponent<ObjectBehaviour>().ChangeDifficulty();
			}
		}
	}
	//! StartSpammingDust() function
	/*! This function is called in order to start spamming "dust" particles setting the flag "spam" to true
	 */
	void StartSpammingDust(){
		spam = true;
	}

	//! StopSpammingDust() function
	/*! This function is called in order to stop spamming "dust" particles setting the flag "spam" to false
	 * and destroys all the "dust" particles. It also broadcasts the function "Scramble()" for a number of random objects
	 * in the room
	 */
	void StopSpammingDust(){
		spam = false;
		spammingTime = 0.0f;
		int numberOfScrambledObjects = Random.Range(1,4);
		int n = 1;
		GameObject[] objects = GameObject.FindGameObjectsWithTag("Objects");
		if(firstScramble){
			foreach (GameObject gameObject in objects){
				if(gameObject.GetComponent<ObjectBehaviour>()){
					gameObject.BroadcastMessage("Scramble");
				}
			}
			firstScramble = false;
		} else{
			foreach (GameObject gameObject in objects){
				if(gameObject.GetComponent<ObjectBehaviour>()){
					gameObject.BroadcastMessage("Scramble");
					if(n == numberOfScrambledObjects)
						break;
					n++;
				}
			}
		}
		GameObject[] dusts = GameObject.FindGameObjectsWithTag("Dust");
		foreach (GameObject dust in dusts){
			Destroy(dust);
		}
	}

	//! DestroyDust() function
	/*! This function is called in order to stop spamming "dust" particles setting the flag "spam" to false
	 * without scrambling the "objects"
	 */
	void DestroyDust(){
		spam = false;
		spammingTime = 0.0f;
		GameObject[] dusts = GameObject.FindGameObjectsWithTag("Dust");
		foreach (GameObject dust in dusts){
			Destroy(dust);
		}
	}

	//! StartReturnTimer() function
	/*! This function is called in order to raise the "returning" flag and start the kid timer
	 */
	void StartReturnTimer(){
		returning = true;
		start = false;
	}

	//! HasReturned() function
	/*! This function is called in order to set the "return" variable in the animation clip to false
	 */
	void HasReturned(){
		anim.SetBool("return",false);
	}

	//! SetCount() function
	/*! This function is called in order to start the count to refresh the position of the 
	 * kid texture on the bar in the GUI
	 */
	void SetCount(){
		Camera.main.GetComponent<CameraBehaviour>().SetCount(true);
	}

	//! Public function PlayAudio()
	/*! This function is called in order to play the "angryClip" audio clip
	 */
	public void PlayAudio(){
		Camera.main.audio.Stop();
		this.audio.Stop();
		this.audio.Play();
		this.audio.PlayOneShot(angryClip);
	}

	//! Public function PlayHappyAudio()
	/*! This function is called in order to play the "happyClip" audio clip
	 */
	public void PlayHappyAudio(){
		Camera.main.audio.Stop();
		this.audio.Stop();
		this.audio.Play();
		this.audio.PlayOneShot(happyClip);
	}

	//! Public function GetReturningTime()
	/*! This function returns a value between 0.0f and 1.0f deoending on the current value of the "returnTime" 
	 */
	public float GetReturningTime(){
		return returnTime/returnTimeLimit;
	}

}
