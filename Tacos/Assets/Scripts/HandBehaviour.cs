﻿using UnityEngine;
using System.Collections;

public class HandBehaviour : MonoBehaviour {

	GameObject arrows;
	public GameObject saucePrefab;
	float previousAngularPosition;
	float currentAngularPosition;
	float speed;
	float h;
	float v;


	// Use this for initialization
	void Start () {
		arrows = GameObject.Find("Arrows");
		previousAngularPosition = 0.0f;
		currentAngularPosition = 0.0f;
		speed = 0.1f;
		h = 0;
		v = 0;
	}
	
	// Update is called once per frame
	void Update () {
		arrows = GameObject.Find("Arrows");
		if(transform.position.x >= 6.0) transform.position = new Vector3(5.99f,transform.position.y,transform.position.z);
		if(transform.position.x <= -6.0) transform.position = new Vector3(-5.99f,transform.position.y,transform.position.z);
		if(transform.position.y >= 5.0) transform.position = new Vector3(transform.position.x,4.99f,transform.position.z);
		if(transform.position.y <= -5.0) transform.position = new Vector3(transform.position.x,-4.99f,transform.position.z);
		h = speed * Input.GetAxis("Mouse X");
		v = speed * Input.GetAxis("Mouse Y");
		if((transform.position.x < 6.0 && transform.position.x > -6.0) && (transform.position.y < 5.0 && transform.position.y > -5.0))
			//transform.position = new Vector3(6.0f*Input.GetAxis("Horizontal"),5.0f*Input.GetAxis("Vertical"));
		transform.Translate(h, v, 0);
	}

	void OnTriggerStay2D(Collider2D other){
		if(other.name == "Arrows" && (bool)arrows){
			if(previousAngularPosition == 0.0f){
				previousAngularPosition = Mathf.Atan2(this.collider2D.bounds.center.y-arrows.transform.position.y,this.collider2D.bounds.center.x-arrows.transform.position.x)*Mathf.Rad2Deg;
				if(previousAngularPosition < 0.0f)
					previousAngularPosition += 360.0f; 
				Instantiate(saucePrefab,this.collider2D.bounds.center,Quaternion.identity);
			}else{
				currentAngularPosition = Mathf.Atan2(this.collider2D.bounds.center.y-arrows.transform.position.y,this.collider2D.bounds.center.x-arrows.transform.position.x)*Mathf.Rad2Deg;
				if(currentAngularPosition < 0.0f)
					currentAngularPosition += 360.0f;
				Debug.Log((currentAngularPosition-previousAngularPosition));
				if((currentAngularPosition-previousAngularPosition) > 10.0f || (currentAngularPosition-previousAngularPosition) < -350.0f){
					previousAngularPosition = currentAngularPosition;
					Instantiate(saucePrefab,this.collider2D.bounds.center,Quaternion.identity);
				}
			}
		}
	}

}
