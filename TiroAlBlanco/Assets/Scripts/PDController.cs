﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//! PDController class
/*! This script works as a filter for the pressure sensor of the gripper input from tracker
 * using hokoma2joystick software and vJoy driver. The filter consists of 2 parts the derivative of
 * 2 time samples of the input and the proportional and current value. The filter considers
 * 2 lower thresholds and 2 upper thresholds for the P-D filter using an hysteresis
 */
public class PDController : MonoBehaviour {

	float upperThreshold; /*!< This variable sets the upper Threshold for the derivative part of the filter */
	float lowerThreshold; /*!< This variable sets the lower Threshold for the derivative part of the filter */
	float previousValue; /*!< Last sample of the input for the derivative part of the filter*/
	float currentValue; /*!< Current sample of the input for the derivative part of the filter */
	float derivative; /*!< The difference between both samples */
	float timeStep; /*!< Time step between both samples (this must be multiples of the step in Fixed update in Unity editor) */
	int steps; /*!< Maximum of desired number of steps in the time window between 2 samples */
	int countSteps; /*!< Count of steps increasing every fixed step */
	bool hold; /*!< Boolean variable is set to true if the gripper is being held meeting the conditions of the filter */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	*/
	void Start () {
		upperThreshold = 0.01f;
		lowerThreshold = -0.01f;
		previousValue = Input.GetAxis("Pressure");
		currentValue = 0.0f;
		derivative = 0.0f;
		timeStep = 0.1f; 
		steps = (int)(timeStep/0.02f);
		countSteps = 0;
		hold = false;
	}

	//! FixedUpdate() function
	/*! This function is called every frame it refreshes the
	 * the control of turns start here as well as the variable
	 * refreshing  once per time step in Unity Editor(0.02 seconds by default)
	 */
	void FixedUpdate () {
		if(countSteps == steps){
			currentValue = Input.GetAxis("Pressure");
			derivative = currentValue - previousValue;
			previousValue = currentValue;
			countSteps = 0;
		}
		if(derivative > upperThreshold && currentValue >= 0.25f){
			hold = true;
		}else if(derivative < lowerThreshold && currentValue < 0.25f){
			hold = false;
		}
		countSteps++;
	}

	//! Public function Hold() 
	/*! Returns the variable "hold"
	 */
	public bool Hold(){
		return hold;
	}

}
