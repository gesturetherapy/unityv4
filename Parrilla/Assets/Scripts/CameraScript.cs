﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

	//private const string typeName = "Barbecue";
	//private const string gameName = "Room1";

	public GUIStyle customBox;
	public GUIStyle customBoxPoints;
	public GUIStyle customTitle;
	public GUIStyle customButton;
	public Texture2D black;
	private static float timeLeftSeconds;
	private static bool paused;
	GameObject meatCreator;
	GameObject meat;
	GameObject spatula;
	static int score;
	static int errors;

	// Use this for initialization
	void Start () {
		score = 0;
		errors = 0;
		Screen.showCursor = false;
		timeLeftSeconds = 60f;
		paused = false;
		Time.timeScale = 1;
		meatCreator = GameObject.Find("MeatCreator");
		spatula = GameObject.Find("Spatula");
		spatula.BroadcastMessage("Pause",paused);
	}

	// Update is called once per frame
	void Update () {
		if(Input.GetButtonDown("Pause Button")){
			if(paused){
				paused = false;
				Time.timeScale = 1;
			} else{
				paused = true;
				Time.timeScale = 0;
			}
			if(spatula) 
				spatula.BroadcastMessage("Pause", paused);
		}
		if(!paused){
			if (timeLeftSeconds <= 0){
				meatCreator.SetActive(false);
				meat =  GameObject.Find("Meat(Clone)");
				if(meat) Destroy(meat);
				Destroy(spatula);
				Screen.showCursor = true;
			}else{
				timeLeftSeconds -= Time.deltaTime;
				if (spatula)
					score = spatula.GetComponent<SpatulaControlScript>().getScore();
			}
		}
	}

	void OnGUI(){
		if(timeLeftSeconds > 0){
			if (!paused){
				Screen.showCursor = false;
				if(!GameObject.Find("Grill").GetComponent<AudioSource>().isPlaying)
					GameObject.Find("Grill").GetComponent<AudioSource>().Play();
				GUI.TextField(new Rect(2*Screen.width/3,Screen.height/10,0,0),"Time: "+timeLeftSeconds.ToString("000"),customBox);
				GUI.TextField(new Rect(Screen.width/10,Screen.height/10,0,0),("Score: "+score.ToString()),customBoxPoints);
			}else{
				Screen.showCursor = true;
				if(GameObject.Find("Grill").GetComponent<AudioSource>().isPlaying)
					GameObject.Find("Grill").GetComponent<AudioSource>().Pause();
				GUI.BeginGroup(new Rect(Screen.width/2-Screen.width/4,Screen.height/2-Screen.height/4,Screen.width/2,Screen.height/2));
				GUI.Box (new Rect(0,0,Screen.width/2,Screen.height/2),"Pause",customTitle);
				if(GUI.Button(new Rect (10,(Screen.height/2-Screen.height/4)/4+100,Screen.width/2-20,Screen.height/8-20), "Resume",customButton)){
					paused = false;
					spatula.BroadcastMessage("Pause", paused);
					Time.timeScale = 1;
				}
				if(GUI.Button (new Rect (10,4*(Screen.height/2-Screen.height/4)/4+20,Screen.width/2-20,Screen.height/8-20), "Restart",customButton)){
					Application.LoadLevel("CountDownScene");
				}
				if(GUI.Button (new Rect (10,6*(Screen.height/2-Screen.height/4)/4,Screen.width/2-20,Screen.height/8-20), "Quit",customButton))
					Application.Quit();
				GUI.EndGroup ();
			}
		}else if (timeLeftSeconds <= 0){
			GameObject.Find("Grill").GetComponent<AudioSource>().Stop();
			GUI.color = new Color32(255, 255, 255, 220);
			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),black);
			GUI.TextField(new Rect(Screen.width/2-2*customBox.fontSize,Screen.height/8,0,0),"Time's up!",customBox);
			GUI.TextField(new Rect(Screen.width/2-4*customBoxPoints.fontSize,4*Screen.height/8,0,0),"Your score is: "+score.ToString(),customBoxPoints);
			//socketServer.StopListening();
			//thread.Abort();
			//thread.Join(); 
		}
	}

	public int getErrors(){
		return errors;
	}

	public void setErrors(int value){
		errors = value;
	}

	public float getTimeleftSeconds(){
		return timeLeftSeconds;
	}

}
