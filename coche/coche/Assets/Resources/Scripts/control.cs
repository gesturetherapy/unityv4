﻿using UnityEngine;
using System.Collections;

public class control : MonoBehaviour
{
// Use this for initialization


static int tires_complete=0;
public bool end_car;
public int movements2screw;
public AudioClip trac_sound, sound_score1,sound_put_tuerca;
public GameObject tuerca_prefab;
//public Sprite flattire;
//public Sprite newtire;

float angleZ = 0;
float mousex = 0;
float mousey = 0;
float [] max_ang = new float[2];
float [] min_ang = new float[2];
float minpos_tuerca,maxpos_tuerca,step_tuerca,tmpf;
Vector3 tuerca_ind_pos;
bool[] rot_right = {false,false,false,false};

//bool rigbody_tuerca;//tuerca_visible;
public int neutral_pos,remove0_put1;
Quaternion rot_avatar,rot_avatar_prev;
Sprite gripper_sprite;
Sprite tire_sprite;
GameObject car_go;
GameObject gripper_ind_go, gripper_ind_bg_go;
GameObject arco_go, colliders_go;
GameObject tuerca_ind_go, birlo_go, tire_go;// tuerca_hex_go;
GameObject [] tuerca_array = new GameObject[6];
GameObject textgui_go, tmp_go, textgui_score_go, textgui_tuercas_go;

string [] path_gripper = new string[2];
string [] txt_gripper_ind = new string[2];
string [] path_tire = new string[2];
Color color_ind, color_grip_bg;
int movements_done;
int id_tuerca;
Vector3 [] arco_pos = new Vector3[2];
Vector3 [] tuercas_pos = new Vector3[6];
//Vector3 tire_pos;
float [] arco_rot = new float[2];
Vector3 [] colliders_pos = new Vector3[2];
float [] colliders_rot = new float[2];

	
void Start (){
	remove0_put1 = 0;
	arco_pos[0] = new Vector3(0.9135267f,0.9397823f,0);
	arco_pos[1] = new Vector3(-0.93f,0.9397823f,0);
	arco_rot[0] = 0f;
	arco_rot[1] = 90f;
	colliders_pos[0] = new Vector3(0.4f,1f,0f);
	colliders_rot[0] = 0;
	colliders_pos[1] = new Vector3(-0.47f,1f,0f);
	colliders_rot[1] = 180f;
	rot_avatar_prev = Quaternion.identity;
	rot_avatar = Quaternion.identity;
	max_ang[0] = 360f;
	min_ang[0] = 260f;
	max_ang[1] = 90f; 
	min_ang[1] = 0f;
	path_gripper[0] = "Sprites/left_gripper";
	path_gripper[1] = "Sprites/right_gripper";
	path_tire[0] = "Sprites/flat_tire";
	path_tire[1] = "Sprites/new_tire";
	txt_gripper_ind[0] = "Girar a la izquierda";
	txt_gripper_ind[1] = "Girar a la derecha";
	neutral_pos = 1;
	end_car = false;
	id_tuerca = 0;
	//tuerca_visible = false;
	color_ind.a=0.75f;
	color_ind.r=0.0f;
	color_ind.g=1.0f;
	color_ind.b=0.0f;

	color_grip_bg.r = 1f;
	color_grip_bg.g = 0.5f;
	color_grip_bg.b = 0f;
	color_grip_bg.a = 1;
				
	birlo_go = GameObject.Find("birlo");
	tuerca_ind_go = GameObject.Find("tuerca_ind");
	minpos_tuerca = birlo_go.renderer.bounds.min.x + 0.5f;
	maxpos_tuerca = birlo_go.renderer.bounds.max.x - 0.6f;
	step_tuerca = (maxpos_tuerca - minpos_tuerca)/((max_ang[neutral_pos] - min_ang[neutral_pos])*(movements2screw/2f));
	tuerca_ind_pos = new Vector3(minpos_tuerca,GameObject.Find("tuerca_ind").transform.position.y,0f);	
	tuerca_ind_go.transform.position = tuerca_ind_pos;
	movements_done = 0;
	
	gripper_sprite= Resources.Load<Sprite>(path_gripper[1]);
	gripper_ind_go = GameObject.Find("gripper_ind");
	gripper_ind_go.GetComponent<SpriteRenderer>().sprite=gripper_sprite;
	
	gripper_ind_bg_go = GameObject.Find("gripper_ind_bg");
	gripper_ind_bg_go.GetComponent<SpriteRenderer>().color=color_grip_bg;

	arco_go = GameObject.Find("arco_der");
	arco_go.GetComponent<SpriteRenderer>().color=color_ind;

	tire_sprite = Resources.Load<Sprite>(path_tire[0]);
	tire_go = GameObject.Find("tire");
	tire_go.GetComponent<SpriteRenderer>().sprite = tire_sprite;
	//tire_pos = tire_go.transform.position;
	//tire_go.GetComponent<Transform>().localScale = new Vector3(0.8f,0.8f,1f);

	tuerca_ind_go = GameObject.Find("tuerca_ind");
	colliders_go = GameObject.Find("collider_angs");
	textgui_go = GameObject.Find("GUI_Text_inst");
	textgui_go.GetComponent<GUIText>().text = "Girar a la derecha";
	
	textgui_score_go = GameObject.Find("GUI_Text_score");
	textgui_score_go.GetComponent<GUIText>().text = "x "+tires_complete;

	textgui_tuercas_go = GameObject.Find("GUI_Text_tuercas");
	textgui_tuercas_go.GetComponent<GUIText>().text = "6/6";

	tuerca_array[0] = GameObject.Find("tuerca0");
	tuerca_array[1] = GameObject.Find("tuerca1");
	tuerca_array[2] = GameObject.Find("tuerca2");
	tuerca_array[3] = GameObject.Find("tuerca3");
	tuerca_array[4] = GameObject.Find("tuerca4");
	tuerca_array[5] = GameObject.Find("tuerca5");

	for(int i=0; i<6;i++){
		tuercas_pos[i] = tuerca_array[i].transform.position;
	}
	//tuerca_hex_go = GameObject.Find("tuerca_hex");
}
//Do Graphic & Input updates here - Update is called once per frame
void Update (){
	//Debug.Log("minpostuerca:"+minpos_tuerca+" postuerca:"+tuerca_ind_);
	textgui_score_go.GetComponent<GUIText>().text = "x "+tires_complete;

	rot_avatar_prev=rot_avatar;
	rot_avatar = Quaternion.Euler (0, 0, Mathf.Round(angleZ));
	
	//if (Input.GetAxis ("Mouse X") != 0 && rot_avatar.eulerAngles.z < max_ang[remove0_put1] && rot_avatar.eulerAngles.z > min_ang[remove0_put1] && !end_car) {
		if ((Input.GetAxis ("Vertical")!=0 || Input.GetAxis ("Horizontal")!=0) && rot_avatar.eulerAngles.z < max_ang[remove0_put1] && rot_avatar.eulerAngles.z > min_ang[remove0_put1] && !end_car) {
		//transform.rotation = Quaternion.Euler (0, 0, Mathf.Round(angleZ));
		transform.rotation = rot_avatar;
		//tuerca_ind_pos = new Vector3(minpos_tuerca+step_tuerca*(-angleZ)*movements_done,GameObject.Find("tuerca_ind").transform.position.y,0f);
		tuerca_ind_pos.x = minpos_tuerca + (step_tuerca*(-angleZ));
		
		if(remove0_put1 == 0 && rot_avatar_prev.eulerAngles.z > rot_avatar.eulerAngles.z)
			tuerca_ind_go.transform.position = tuerca_ind_pos;
		if(remove0_put1 == 1 && rot_avatar_prev.eulerAngles.z < rot_avatar.eulerAngles.z)
			tuerca_ind_go.transform.position = tuerca_ind_pos;
		

		if(tuerca_array[id_tuerca]!=null)
			tuerca_array[id_tuerca].transform.rotation = rot_avatar;
	}
	arco_go.GetComponent<SpriteRenderer>().color=color_ind;
	//gripper_ind_bg_go.GetComponent<SpriteRenderer>().color=color_ind;
	//if(neutral_pos==1){
		gripper_sprite= Resources.Load<Sprite>(path_gripper[neutral_pos]);
		gripper_ind_go.GetComponent<SpriteRenderer>().sprite=gripper_sprite;
		textgui_go.GetComponent<GUIText>().text = txt_gripper_ind[neutral_pos];
	//}	
	//if(rot_right[3]){
	//	gripper_sprite= Resources.Load<Sprite>(path_gripper[0]);
	//	gripper_ind_go.GetComponent<SpriteRenderer>().sprite=gripper_sprite;
	//	textgui_go.GetComponent<GUIText>().text = "Girar a la izquierda";
	//}
	
	if ( movements_done == movements2screw){// && rigbody_tuerca){
		if(remove0_put1==0)
			minpos_tuerca = birlo_go.renderer.bounds.min.x + 0.5f;	
		else
			minpos_tuerca = birlo_go.renderer.bounds.max.x - 0.6f;	
		tuerca_ind_pos = new Vector3(minpos_tuerca,GameObject.Find("tuerca_ind").transform.position.y,0f);	
		tuerca_ind_go.transform.position = tuerca_ind_pos;
		if (remove0_put1==0){
			//tuerca_hex_go.AddComponent<Rigidbody2D>();
			//tuerca_hex_go.GetComponent<Rigidbody2D>().isKinematic=false;
			tuerca_array[id_tuerca].AddComponent<Rigidbody2D>();
			//rigbody_tuerca = false;
			movements_done = 0;			
			id_tuerca++;
			textgui_tuercas_go.GetComponent<GUIText>().text = (6-id_tuerca)+"/6";
			
		}
		else{
			tuerca_array[id_tuerca] = Instantiate(tuerca_prefab,tuercas_pos[id_tuerca],transform.rotation) as GameObject;
			tuerca_array[id_tuerca].name = "tuerca"+id_tuerca.ToString();
			id_tuerca++;
			audio.PlayOneShot(sound_put_tuerca);
			textgui_tuercas_go.GetComponent<GUIText>().text = (6-id_tuerca)+"/6";
			movements_done=0;
			//Crear la i-esima tuerca en la posicion tuerca_pos[i] y dar brillo
			// Reparar lo del color
			// si se han puesto todas las tuercas, mover el carro.			
		}
	}
	if (id_tuerca == 6){

		if(remove0_put1==0){
			id_tuerca = 0;
			remove0_put1 = 1 - remove0_put1;
			arco_go.GetComponent<Transform>().position = arco_pos[remove0_put1];
			arco_go.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0,0,arco_rot[remove0_put1]));
			colliders_go.GetComponent<Transform>().position = colliders_pos[remove0_put1];
			colliders_go.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0,colliders_rot[remove0_put1],0));
			neutral_pos = 1 -neutral_pos;
			tmpf = minpos_tuerca;
			minpos_tuerca = maxpos_tuerca;
			maxpos_tuerca = tmpf;
				step_tuerca = (maxpos_tuerca - minpos_tuerca)/((max_ang[neutral_pos] - min_ang[neutral_pos])*(-1)*(movements2screw/2f));
				tuerca_ind_pos = new Vector3(minpos_tuerca,GameObject.Find("tuerca_ind").transform.position.y,0f);
			tire_go.transform.rotation = Quaternion.Euler(new Vector3(0,0,Random.Range(20,80)));
			//tire_go.AddComponent<Rigidbody2D>();
			tire_go.rigidbody2D.isKinematic=false;
			end_car = true;
			textgui_tuercas_go.GetComponent<GUIText>().text = "6/6";
			tuerca_ind_pos = new Vector3(minpos_tuerca,GameObject.Find("tuerca_ind").transform.position.y,0f);	
			tuerca_ind_go.transform.position = tuerca_ind_pos;
		}
		else{
			id_tuerca = 0;
			remove0_put1 = 1 - remove0_put1;
			arco_go.GetComponent<Transform>().position = arco_pos[remove0_put1];
			arco_go.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0,0,arco_rot[remove0_put1]));
			colliders_go.GetComponent<Transform>().position = colliders_pos[remove0_put1];
			colliders_go.GetComponent<Transform>().rotation = Quaternion.Euler(new Vector3(0,colliders_rot[remove0_put1],0));
			neutral_pos = 1 -neutral_pos;
			tmpf = minpos_tuerca;
			minpos_tuerca = maxpos_tuerca;
			maxpos_tuerca = tmpf;
			step_tuerca = (maxpos_tuerca - minpos_tuerca)/((max_ang[neutral_pos] - min_ang[neutral_pos])*(-1)*(movements2screw/2f));
			tuerca_ind_pos = new Vector3(minpos_tuerca,GameObject.Find("tuerca_ind").transform.position.y,0f);
			tires_complete++;
			audio.PlayOneShot(sound_score1);
			end_car = true;
			//Destroy(GameObject.Find("jack"));
			for( int i=0;i<6;i++){
				Destroy(tuerca_array[i]);
			}
			car_go = GameObject.Find("car_body");
			//Resources.Load<Sprite>("car_normal");
			car_go.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/car_normal");
			car_go.transform.rotation=Quaternion.Euler(new Vector3(0,0,0));
			//Destroy(GameObject.Find("tire"));
			GameObject.Find("tire").GetComponent<Renderer>().renderer.enabled = false;
			GameObject.Find("jack").GetComponent<Renderer>().renderer.enabled = false;
			GameObject.Find("matraca_bg").GetComponent<Renderer>().renderer.enabled = false;
			GameObject.Find("matraca").GetComponent<Renderer>().renderer.enabled = false;
			GameObject.Find("arco_der").GetComponent<Renderer>().renderer.enabled = false;
			GameObject.Find("tuerca_hex").GetComponent<Renderer>().renderer.enabled = false;
			remove0_put1 = 1 - remove0_put1;
			id_tuerca = 0;
			
			//yield return new WaitForSeconds(5);

			Application.LoadLevel("scene_salida");
		}
	}
	
}

void OnTriggerEnter2D (Collider2D other)
{
	if (other.gameObject.name == "zerodeg") {
		audio.PlayOneShot(trac_sound);
		gripper_ind_bg_go.GetComponent<SpriteRenderer>().color=color_grip_bg;
	}else{
		if (other.gameObject.name == "firstdeg") {
			audio.PlayOneShot(trac_sound);
			gripper_ind_bg_go.GetComponent<SpriteRenderer>().color=Color.red;
			if(rot_right[3]==true){
				neutral_pos = 1 - neutral_pos;
				rot_right[3]=false;
				movements_done++;
			}
			else{
				rot_right [0] = true;
			}
		}
		if (other.gameObject.name == "secdeg") {
			audio.PlayOneShot(trac_sound);
			if(rot_right[2]==true){
				rot_right[1]=false;
			}
			else{
				rot_right [1] = true;
			}
		}
		if (other.gameObject.name == "thirddeg"){
			audio.PlayOneShot(trac_sound);
			gripper_ind_bg_go.GetComponent<SpriteRenderer>().color=Color.red;
			if(rot_right[3]==true){
				rot_right[2]=false;
				minpos_tuerca = tuerca_ind_go.transform.position.x;
			}
			else{
				rot_right [2] = true;
			}
		}
		if (other.gameObject.name == "fourthdeg"){
			audio.PlayOneShot(trac_sound);
			if(rot_right[0]==true && rot_right[3]==false){
				neutral_pos = 1 - neutral_pos;
				rot_right [3] = true;
				movements_done++;
				gripper_ind_bg_go.GetComponent<SpriteRenderer>().color=color_grip_bg;
			}
		}
		
	}
		//Debug.Log("mov_done:"+movements_done);
}
// Do physics engine updates here
void FixedUpdate () {
/*
		if (Input.GetAxis ("Mouse X") != 0) {
			mousex = Input.mousePosition.x - Screen.width / 2f;
			mousey = Input.mousePosition.y - Screen.height / 2f;
			angleZ = Mathf.Atan2 (mousey, mousex) * Mathf.Rad2Deg - 90;
		}
*/
		mousex = GameObject.Find("move_axis").GetComponent<move_with_axis>().xpix;
		mousey = GameObject.Find("move_axis").GetComponent<move_with_axis>().ypix;
		angleZ = Mathf.Atan2 (mousey, mousex) * Mathf.Rad2Deg - 90;

		//Debug.Log("x:"+mousex+" y:"+mousey+" angZ:"+angleZ+" rot:"+transform.rotation.eulerAngles);
		Debug.Log(Input.GetAxis("Mouse X"));
		if (Mathf.Abs(angleZ)>0 && Mathf.Abs(angleZ)<=45){
			color_ind.r =  Mathf.Abs(angleZ) / 45f;
		}
		if (Mathf.Abs(angleZ)>45 && Mathf.Abs(angleZ)<=90){
			color_ind.g =  Mathf.Abs(Mathf.Abs(angleZ)-90) / 44f;
		}
}
 
}
