﻿using UnityEngine;
using System.Collections;

public class move_car : MonoBehaviour {

	public float speed_car;
	public AudioClip start_car;
	// Use this for initialization
	bool seen = false;

	void Update(){
		if(renderer.isVisible && seen==false){
			seen = true;
			audio.PlayOneShot(start_car);
			
		}
		if(renderer.isVisible && seen){
			transform.Translate(new Vector3(speed_car,0,0)*Time.deltaTime);
			if (Time.frameCount%12 == 0) {
				transform.rotation = Quaternion.Euler(new Vector3(0,0,0));	
				transform.rotation = Quaternion.Euler(new Vector3(0,0,Random.Range(-1,1)));	
				speed_car++;
			}
		}
		if(!renderer.isVisible && seen)
			Application.LoadLevel("scene_entrada");				
	}
}