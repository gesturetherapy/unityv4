﻿using UnityEngine;
using System.Collections;

public class tuerca_control : MonoBehaviour {

	// Use this for initialization
	public AudioClip bounce_sound;
	bool seen = false;
	void Update(){
		if(renderer.isVisible)
			seen = true;
		if(!renderer.isVisible && seen){
			seen = false;
			Destroy(gameObject);
		}		
	}

	void OnCollisionEnter2D (Collision2D other){
		audio.PlayOneShot(bounce_sound);
	}
	
}
