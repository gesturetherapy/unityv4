﻿using UnityEngine;
using System.Collections;

public class CountDownGUI : MonoBehaviour {
	
	private static float countdown;
	public GUIStyle style;
	public Texture2D black;
	private static byte alpha;
	private static string previousValue;


	// Use this for initialization
	void Start () {
		countdown = 3.5f;
		if (Time.timeScale == 0)
			Time.timeScale = 1;
		alpha = 255;
		previousValue = countdown.ToString("0");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if(alpha > 5)
			alpha -= 5;
		if(countdown.ToString("0") != previousValue){
			alpha = 255;
			previousValue = countdown.ToString("0");
		}
		if(countdown > -0.49){
			countdown -= Time.fixedDeltaTime;
		} else {
			Application.LoadLevel("scene_entrada");
		}
	}

	void OnGUI(){
		GUI.color = new Color32(255, 255, 255, 255);
		GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height),black);
		GUI.color = new Color32(255, 255, 255, alpha);
		GUI.TextField(new Rect(0,0,Screen.width,Screen.height),countdown.ToString("0"),style);
	}

}
