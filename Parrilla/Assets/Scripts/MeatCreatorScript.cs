﻿using UnityEngine;
using System.Collections;

public class MeatCreatorScript : MonoBehaviour {

	public GameObject meat;
	GameObject meatClone;

	// Use this for initialization
	void Start () {
		meatClone = (GameObject) Instantiate(meat);
		meatClone.transform.position = new Vector3((6*Random.value)-3,Random.value,0);
	}
	
	// Update is called once per frame
	void Update () {
		if(!meatClone){	
			meatClone = (GameObject) Instantiate(meat);
			meatClone.transform.position = new Vector3((6*Random.value)-3,Random.value,0);
		}
	}

}
