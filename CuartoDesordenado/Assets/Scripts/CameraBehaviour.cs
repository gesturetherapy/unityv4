﻿using UnityEngine;
using System.Collections;

//! cameraBehaviour class
/*! This class describes the behaviour of most of the GUI of the game,
 * the time left in seconds of the game, it also handles when to change
 * the difficulty of the game and the object destroying after the game
 */
public class CameraBehaviour : MonoBehaviour {

	public GUIStyle scoreStyle; /*!< This variable represents the style of the text for the GUI of the score */
	public Texture barEmpty; /*!< This texture represents the bar for the returning time of the "kid" GameObject */
	public Texture pressureBarEmpty; /*!< This variable represents the pressure bar for the "Gripper" pressure input */
	public Texture pressureBar; /*!< This texture overlaps the empty bar for the pressure sensor depending on the current value */
	public Texture kidTexture; /*!< The texture of the kid that fills the bar depending on the returning time of the "kid" */
	public Texture dustTexture; /*!< This texture fills the bar depending on the returning time of the "kid" */
	public Texture openHand; /*!< This texture is part of the GUI that helps the user to follow the instructions */
	public Texture closedHand; /*!< This texture is part of the GUI that helps the user to follow the instructions */
	public AudioClip alarmClip; /*!< This audio clip is played when the kid timer is near to the end */
	GameObject kid; /*!< This variable represents the kid and returning time */
	int score; /*!< This variable is the current score and it's shown in the GUI */
	float position; /*!< The current position of the kid texture at the bar */
	bool count; /*!< This flag is raised to refresh the position of the kid texture in the GUI bar */
	bool playable; /*!< This flag is raised when there's no current Audio clip playing */
	float onGUITime; /*!< This variable represents the timer for showing some elements in the GUI */
	bool GUIcount; /*!< This flag is raised in order to show some elements in the GUI */
	float timeLeftSeconds; /*!< This variable represents the remaining time to finish the game */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	*/
	void Start () {
		kid = GameObject.Find("Kid");
		score = 0;
		position = kid.GetComponent<KidBehaviour>().GetReturningTime();
		count = true;
		playable = true;
		onGUITime = 0.0f;
		GUIcount = false;
		timeLeftSeconds = 180.0f;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * time countdown, timers and GUI flag controller 
	 * like the kid texture bar
	 */
	void Update () {
		timeLeftSeconds -= Time.deltaTime;
		if(timeLeftSeconds <= 0.0f)
			Application.Quit();
		if(count)
			if(kid.GetComponent<KidBehaviour>().GetReturningTime() <= 0.98f)
				position = kid.GetComponent<KidBehaviour>().GetReturningTime();
			else
				count = false;
		if(GUIcount){
			onGUITime += Time.deltaTime;
			if (onGUITime >= 0.5f)
				onGUITime = 0.0f;
		}
	}

	//! Public function AddScore()
	/*! This function is called in order to refresh the score
	 * after an "object" GameObject is set in their goal position
	 */
	public void AddScore(int points){
		score += points;
	}

	//! Public function SetCount()
	/*! This function sets the value to the variable "count" in order to
	 * refresh the position of the kid texture on the bar 
	 */
	public void SetCount(bool value){
		count = value;
	}

	//!OnGUI() function
	/*! This script contains the most of the GUI and feedback of the game, this includes 2d textures,
	 * scores, time remaining, audio to play and instructions with 2D textures. Most of this GUI layer
	 * is planned for a resolution of 1024*768
	 */
	void OnGUI(){
		if(timeLeftSeconds >= 0.0f)
			GUI.TextArea(new Rect(Screen.width/2-125,10,250,50),"Time: "+timeLeftSeconds.ToString("000"),scoreStyle);
		GUI.DrawTexture(new Rect(Screen.width/2-145,70,pressureBarEmpty.width/3,pressureBarEmpty.height/3),pressureBarEmpty);
		GUI.color = new Color(1,1-((Input.GetAxis("Pressure")+1.0f)/2),0);
		GUI.DrawTexture(new Rect(Screen.width/2-145,70,((Input.GetAxis("Pressure")+1.0f)/2)*pressureBarEmpty.width/3,pressureBarEmpty.height/3),pressureBar);
		GUI.color = Color.white;
		GUI.DrawTexture(new Rect(Screen.width/2-165,75,openHand.width/3,openHand.height/3),openHand);
		GUI.DrawTexture(new Rect(Screen.width/2-170+pressureBarEmpty.width/3,75,closedHand.width/3,closedHand.height/3),closedHand);
		GUI.TextArea(new Rect(Screen.width-310,70,300,50),"Score: "+score.ToString(),scoreStyle);
		if(position < 0.8){
			GUIcount = false;
			GUI.DrawTexture(new Rect(Screen.width-310,10,300,50),barEmpty);
			playable = true;
		}
		else if (position >= 0.8){
			GUIcount = true;
			GUI.color = new Color(1,1-((position-0.8f)/0.2f),0);
			if(onGUITime > 0.25f){
				GUI.DrawTexture(new Rect(Screen.width-310,10,300,50),barEmpty);
				if(playable){
					this.audio.Stop();
					this.audio.Play();
					this.audio.PlayOneShot(alarmClip);
					playable = false;
				}
			}
		}
		GUI.color = new Color(1,1,1,0.75f);
		if(position > 0.2f){
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.0f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.1f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
		}
		if(position > 0.4f){
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.2f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.3f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
		}
		if(position > 0.6f){
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.4f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.5f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
		}
		if(position > 0.8f){
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.6f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.7f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
		}
		if(position > 0.9f){
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.8f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
			GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(0.9f*300),10,kidTexture.width/2,kidTexture.height/2),dustTexture);
		}
		GUI.color = new Color(1,1,1,1);
		GUI.DrawTexture(new Rect(Screen.width-10-(kidTexture.width/4)-(position*300),10,kidTexture.width/2,kidTexture.height/2),kidTexture);
		}
}
