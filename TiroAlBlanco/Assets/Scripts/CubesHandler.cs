﻿using UnityEngine;
using System.Collections;

//! CubesHandler Class
/*!
 * This class checks the current velocity of the cubes
 * in order to know if they're still in movement and 
 * finish the attempt of destroying the cubes. This class 
 * also describes when to ofinish a turn and start another one
*/

public class CubesHandler : MonoBehaviour {

	GameObject[] currentCubes;  /*!< Array of current GameObjects "Cubes" */
	GameObject[] startingCubes; /*!< Array of starting GameObjects "Cubes" */
	GameObject aim; /*!< GameObject of the avatar "aim" */
	GameObject hand; /*!< Child GameObject "hand" of the avatar */
	bool turnEnded; /*!< Boolean variable describing if the cubes are in movement */
	int destroyed_turn; /*!< Number of GameObjects "Cubes" destroyed in that turn */
	int destroyed; /*!< Total number of GameObjects "Cubes" destroyed in all the turns */
	float multiplier; /*!< This variable depends on the number of cubes destroyed in that turn */
	public AudioClip multiplierClip; /*!< Audio playing when multiplier is shown in the GUI */
	public GUIStyle customBox; /*!< GUIStyle used in the OnGUI() function for the multiplier */
	bool showCombo; /*!< Boolean variable indicating then the multiplier would be shown in the OnGUI() function*/
	float comboCounter; /*!< This vaariable describes how much time would the combo multiplier stay on screen */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	*/
	void Start () {
		startingCubes = GameObject.FindGameObjectsWithTag("Cubes");
		aim = GameObject.Find("Aim");
		hand = GameObject.Find("Hand");
		currentCubes = startingCubes;
		turnEnded = false;
		destroyed_turn = 0;
		destroyed = 0;
		multiplier = 0.0f;
		showCombo = false;
		comboCounter = 0.0f;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * the control of turns start here as well as the variable
	 * refreshing
	 */
	void Update () {
		//! Check if game has finished
		/*! When there are no cubes left in the scene the the game goes to the next scene using GameSucceed() function */
		if(currentCubes.Length == 0){
			Camera.main.GetComponent<CameraBehaviour>().GameSucceed();
		}
		//! Refresh current number of cubes
		/*! Refresh the number of current cubes on the scene each frame
		 * and check for the remaining
		*/
		currentCubes = GameObject.FindGameObjectsWithTag("Cubes");
		destroyed = startingCubes.Length - currentCubes.Length;
		//! End of turn handle
		/*! Checks if turn has ended and set some variables to their initial values, 
		 * when all the cubes aren't moving the end of the turn finishes and the
		 * score is updated, the avatar can interact again with the game, 
		 * the remaining time is stopped until the cubes aren't moving and
		 * the animation of the avatar ends.
		 */
		if(turnEnded){
			hand.GetComponent<HandBehaviour>().setInteractive(false);
			aim.GetComponent<AimControllerScript>().setForce(aim.GetComponent<AimControllerScript>().minForce);
			aim.GetComponent<AimControllerScript>().setInteractive(false);
			hand.GetComponent<HandBehaviour>().ResetPositionScale();
			Camera.main.GetComponent<CameraBehaviour>().Stop(true);
			if(areStatic(currentCubes)){
				destroyed_turn -= currentCubes.Length;
				multiplier = CalculateMultiplier(destroyed_turn);
				if(multiplier >= 2.0){
					this.audio.PlayOneShot(multiplierClip);
					showCombo = true;
				}
				aim.GetComponent<AimControllerScript>().UpdateScore(multiplier);
				hand.GetComponent<HandBehaviour>().setInteractive(true);
				aim.GetComponent<AimControllerScript>().setInteractive(true);
				Camera.main.GetComponent<CameraBehaviour>().Stop(false);
				turnEnded = false;
			}
		}
		//! GUI score feedback
		/*! The score is updated multiplied by the "multiplier" variable depending
		 * on the number of cubes destroyed during that turn, then the multiplier is shown in
		 * the OnGUI() function
		 */
		if(showCombo)
			comboCounter += Time.deltaTime;
		else
			comboCounter = 0.0f;
	}

	//! areStatic() function
	/*! When called it checks the linear velocity and angular velocity of the GameObject
	 * array "cubes", it returns false when at least a cube has a velocity higher than 0.01 units 
	 */
	bool areStatic(GameObject[] cubes){
		bool check;
		check = true;
		foreach(GameObject cube in cubes){
			if(cube.rigidbody2D.velocity.magnitude > 0.01f || cube.rigidbody2D.angularVelocity > 0.01f){
				check = false;
				break;
			}
		}
		return check;
	}

	//! Public function getDestroyed()
	/*! Returns the number of GameObjects "cubes" destroyed during the whole gameplay */
	public int getDestroyed(){
		return destroyed;
	}

	//! Public funtion StartEndOfTurn()
	/*! This function sets the variable "turnEnded" to true, allowing the beginning
	 * of the end of turn procedure deescribed in Update() function
	 */
	public void StartEndOfTurn(){
		destroyed_turn = currentCubes.Length;
		turnEnded = true;
	}

	//! CalculateMultiplier() function
	/*! Returns the value of the multiplier depending of the numer of GameObjects "cube" destroyed
	 * during that turn, if 4 or more cubes are destroyed the return value is 5.0f
	 */
	float CalculateMultiplier(int destroyed){
		if(destroyed == 0){
			return 0.0f;
		}else if(destroyed == 1){
			return 1.0f;
		}else if(destroyed == 2){
			return 2.0f;
		}else if(destroyed == 3){
			return 3.0f;
		}else if(destroyed == 4){
			return 4.0f;
		}else{
			return 5.0f;
		}
	}

	//! Public function getMultiplier()
	/*! Returns the current value of the variable "multiplier" */
	public float getMultiplier(){
		return multiplier;
	}

	//! OnGUI() function
	/*! if variable "showCombo" is set to true the variable "multiplier" is shown each frame as a 
	 * feedback for the user, this GUI message uses the GUIStyle "customBox" and lasts 2 seconds
	 */
	void OnGUI(){
		if(showCombo){
			if(comboCounter <= 2.0f)
				GUI.TextField(new Rect(Screen.width/2,2.5f*Screen.height/10,0,0),"Combo! x"+multiplier.ToString("0.0"),customBox);
			else
				showCombo = false;
		}
	}

}
