﻿using UnityEngine;
using System.Collections;
using System.Threading;
using System.Collections.Generic;
using MiniJSON;

public class Serializer : MonoBehaviour {
	string[] sensors = {"vJoy Device"};
	string[] avatars = {"Spatula"};
	string[] distractors = {"Grill", "Griddle", "Grass", "Fence", "Sky", "Cloud"};
	string[] feedback = {"Fire"};
	string[] targets = {"Meat"};
	Hashtable variables = new Hashtable();
	private static List<JSON[]> eventList = new List<JSON[]>();
	private static string serial;
	//private static AsyncSocketServer.AsynchronousSocketListener socketServer = new AsyncSocketServer.AsynchronousSocketListener();
	//private static Thread thread = new Thread(new ThreadStart(Listen));

	// Use this for initialization
	void Start () {
		/*if(!thread.IsAlive)
			thread.Start();
		while (!thread.IsAlive);*/
	}
	
	// Update is called once per frame
	void Update () {
		if(GameObject.Find("Spatula")) // ---------------------------------------------------------------------------------TEMPORAL!!! 
			variables.Add("score",GameObject.Find("Spatula").GetComponent<SpatulaControlScript>().getScore());
		if((Camera) FindObjectOfType(typeof(Camera))){
			variables.Add("errors",((Camera) FindObjectOfType(typeof(Camera))).GetComponent<CameraScript>().getErrors());
			variables.Add ("timeLeftSeconds",((Camera) FindObjectOfType(typeof(Camera))).GetComponent<CameraScript>().getTimeleftSeconds());
		}
		serial = Serialize("Barbecue",sensors, avatars, distractors, feedback, targets, variables);
		//socketServer.UpdateSerial(serial);
		using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"C:\Users\Walther\Documents\SensorUSB\Capture\WriteLines.txt",true)){
			file.WriteLine(serial);
		}
	}

	private string Serialize(string gameName,string[] sensorNames, string[] avatarNames, string[] distractorNames, string[] feedbackNames, string[] targetNames, Hashtable variableNames){
		string jsonString = "";
		JSON js = new JSON();
		js["time stamp"] = Time.time.ToString();
		js["game"] = gameName;
		js["interaction"] = "1.0";
		string[] joysticks = Input.GetJoystickNames();
		js["sensors"] = addInputs(joysticks,sensorNames);
		Object [] objects = FindObjectsOfType(typeof(GameObject));
		js["avatars"] = addObjects(objects,avatarNames);
		js["distractors"] = addObjects(objects,distractorNames);
		js["feedbacks"] = addComponents(objects,feedbackNames);
		js["targets"] = addObjects(objects,targetNames);
		js["variables"] = addVariables(variableNames);
		variableNames.Clear();
		js["events"] = eventList.ToArray();
		eventList.Clear();
		jsonString = js.serialized;
		return jsonString;
	}
	
	private JSON[] addObjects(Object[] objects ,string[] names){
		List<JSON> objectsList = new List<JSON>();
		foreach(Object gameObject in objects){
			List<JSON> collidersList = new List<JSON>();
			for (int i = 0; i < names.Length; i++){
				if (gameObject.name.StartsWith(names[i])){
					JSON tempObject = new JSON();
					if(gameObject.name.EndsWith("(Clone)")){
						tempObject["name"] = gameObject.name.Remove(gameObject.name.IndexOf("("));
					} else{
						tempObject["name"] = gameObject.name;
					}
					tempObject["id"] = gameObject.GetInstanceID().ToString();
					if (((GameObject)gameObject).GetComponent<Renderer>()) {
						tempObject["hidden"] = (!((GameObject)gameObject).GetComponent<Renderer>().isVisible).ToString(); 
					}else if (((GameObject)gameObject).GetComponent<SpriteRenderer>()) {
						tempObject["hidden"] = (!((GameObject)gameObject).GetComponent<SpriteRenderer>().isVisible).ToString();
					}
					Vector3 onScreenCheck = this.camera.WorldToScreenPoint(((GameObject)gameObject).transform.position);
					if((onScreenCheck.x >= 0 && onScreenCheck.x <= Screen.width) && (onScreenCheck.y >= 0 && onScreenCheck.y <= Screen.height)){
						tempObject["on_sceen"] = "True"; 
					} else{
						tempObject["on_sceen"] = "False";
					}
					tempObject["enabled"] = ((bool)((GameObject)gameObject).GetComponent<Renderer>()).ToString();
					if (((GameObject)gameObject).GetComponent<Rigidbody>()){
						tempObject["static"] = ((GameObject)gameObject).GetComponent<Rigidbody>().isKinematic.ToString(); 
					} else if (((GameObject)gameObject).GetComponent<Rigidbody2D>()){
						tempObject["static"] = ((GameObject)gameObject).GetComponent<Rigidbody2D>().isKinematic.ToString();
					}
					tempObject["position"] = (JSON)(((GameObject)gameObject).transform.position);
					tempObject["rotation"] = (JSON)(((GameObject)gameObject).transform.localRotation);
					if (((GameObject)gameObject).transform.localScale == new Vector3(1,1,1)){
						tempObject["scale"] = "none";
					} else{
						tempObject["scale"] = ((GameObject)gameObject).transform.localScale.ToString();
					}
					if (((GameObject)gameObject).GetComponent<Rigidbody>()){
						tempObject["linear_velocity"] = ((GameObject)gameObject).GetComponent<Rigidbody>().velocity.ToString();
						tempObject["angular_velocity"] = ((GameObject)gameObject).GetComponent<Rigidbody>().angularVelocity.ToString();
					} else if (((GameObject)gameObject).GetComponent<Rigidbody2D>()){
						tempObject["linear_velocity"] = ((GameObject)gameObject).GetComponent<Rigidbody2D>().velocity.ToString();
						tempObject["angular_velocity"] = ((GameObject)gameObject).GetComponent<Rigidbody2D>().angularVelocity.ToString();
					}
					tempObject["id"] = gameObject.GetInstanceID().ToString();
					if (((GameObject)gameObject).GetComponent<Renderer>()) {
						tempObject["size"] = ((GameObject)gameObject).GetComponent<Renderer>().bounds.size.ToString(); 
					}else if (((GameObject)gameObject).GetComponent<SpriteRenderer>()) {
						tempObject["size"] = ((GameObject)gameObject).GetComponent<SpriteRenderer>().bounds.size.ToString(); 
					}
					Collider [] colliders = ((GameObject)gameObject).GetComponents<Collider>();
					foreach(Collider collider in colliders){
						JSON tempCollider = new JSON();
						if (collider.GetType() == typeof(BoxCollider)){
							tempCollider["type"] = "box_collider";
							tempCollider["bounds"] = collider.bounds.center.ToString()+","+collider.bounds.extents.ToString();
							tempCollider["trigger"] = collider.isTrigger.ToString();
							collidersList.Add(tempCollider); 
						} else if(collider.GetType() == typeof(SphereCollider)){
							tempCollider["type"] = "sphere_collider";
							tempCollider["bounds"] = collider.bounds.center.ToString()+","+collider.bounds.extents.ToString();
							tempCollider["trigger"] = collider.isTrigger.ToString();
							collidersList.Add(tempCollider); 
						} else if(collider.GetType() == typeof(CapsuleCollider)){
							tempCollider["type"] = "capsule_collider";
							tempCollider["bounds"] = collider.bounds.center.ToString()+","+collider.bounds.extents.ToString();
							tempCollider["trigger"] = collider.isTrigger.ToString();
							collidersList.Add(tempCollider);  
						}else{
							tempCollider["type"] = "unsupported_collider";
							collidersList.Add(tempCollider); 
						}
					}
					Collider2D [] colliders2D = ((GameObject)gameObject).GetComponents<Collider2D>();
					foreach(Collider2D collider2D in colliders2D){
						JSON tempCollider2D = new JSON();
						if (collider2D.GetType() == typeof(BoxCollider2D)){
							tempCollider2D["type"] = "box_collider2D";
							tempCollider2D["bounds"] = collider2D.bounds.center.ToString()+","+collider2D.bounds.extents.ToString();
							tempCollider2D["trigger"] = collider2D.isTrigger.ToString();
							collidersList.Add(tempCollider2D);  
						} else if(collider2D.GetType() == typeof(CircleCollider2D)){
							tempCollider2D["type"] = "circle_collider2D";
							tempCollider2D["bounds"] = collider2D.bounds.center.ToString()+","+collider2D.bounds.extents.ToString();
							tempCollider2D["trigger"] = collider2D.isTrigger.ToString();
							collidersList.Add(tempCollider2D); 
						} else{
							tempCollider2D["type"] = "unsupported_collider2D";
							collidersList.Add(tempCollider2D); 
						}
					}
					if (collidersList.ToArray().Length == 0){
						tempObject["colliders"] = "none";
					}else{
						tempObject["colliders"] = collidersList.ToArray();
					}
					objectsList.Add(tempObject);
					break;
				}
			}
		}
		return objectsList.ToArray();
	}
	
	private JSON[] addInputs(string [] inputs,string[] names){
		List<JSON> inputList = new List<JSON>();
		for(int i = 0; i < inputs.Length; i++){	
			for(int j = 0; j < names.Length; j++){
				if(inputs[i].StartsWith(names[j])){
					JSON tempInput = new JSON();
					tempInput["name"] = inputs[i];
					inputList.Add(tempInput);
					break;
				}
			}
		}
		return inputList.ToArray();
	}
	
	private JSON[] addComponents(Object [] objects, string[] names){
		List<JSON> componentList = new List<JSON>();
		JSON tempComponent = new JSON();
		foreach(Object gameObject in objects){
			AudioSource[] audioSources = ((GameObject)gameObject).GetComponents<AudioSource>();
			foreach(AudioSource audioSource in audioSources){
				for(int i = 0; i < names.Length; i++){
					if(audioSource.clip.name.StartsWith(names[i])){
						tempComponent = new JSON();
						tempComponent["type"] = "audio";
						tempComponent["name"] = audioSource.clip.name;
						tempComponent["playing"] = audioSource.isPlaying.ToString();
						tempComponent["volume"] = audioSource.volume.ToString();
						componentList.Add(tempComponent);
						break;
					}
				}
			}
		}
		return componentList.ToArray();
	}

	private JSON addVariables(Hashtable names){
		JSON tempVariable = new JSON();
		foreach(DictionaryEntry variable in names){
			tempVariable[variable.Key.ToString()] = variable.Value;
		}
		return tempVariable;
	}

	private void addEventList(List<JSON> serialEventList){
		eventList.Add(serialEventList.ToArray());
	}
	
	/*public static void Listen(){
		socketServer.StartListening();
	}
	
	void OnApplicationQuit() {
		if(thread.IsAlive){
			socketServer.StopListening();
			thread.Abort();
			thread.Join();
		}
	}*/

}
