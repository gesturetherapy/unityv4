﻿using UnityEngine;
using System.Collections;

//! ObjectBehaviour class
/*! This class describes the behaviour of every "object" GameObject. The main objective of the game
 * is that the user has to put every "object" in their goal position or their respective slot called
 * "thisSlot". The object can appear in a random area delimited by a rectangle after a call of the "Scramble"
 * function
 */
public class ObjectBehaviour : MonoBehaviour {

	Vector2 goalPosition; /*!< This variable represents the goal destination of the objects */
	Vector2 scrambledPosition; /*!< This variable represents the scrambled position of the objects and sets some difficulty*/
	Vector3 scrambledOrientation; /*!< This variable represents the orientation of the "object" at their scrambled position */
	public string objectTag; /*!< This variable represents the unique object type identifier of each "object" GameObject, i.e. books */
	public Rect scrambleRange; /*!< This variable represents the delimited area in which the "object" will be instantiated after the "kid" scramble this "object" */
	Rect startingScrambleRange; /*!< This variable represents the initial area for instantiation */
	int difficulty; /*!< This variable represents the current difficulty  of the game */
	public GameObject slot; /*!< This GameObject prefab represents the goal position to release this "object"  */
	public GameObject otherSlot; /*!< This GameObject prefab represents the goal position to release the other "objects" */
	public GameObject score10; /*!< This prefab is instantiated after an "object" is succesfully released at its corresponding goal position or slot */
	public AudioClip pointingClip; /*!< This audio clip is played when an "object" is released at its corresponding goal position or slot */
	public AudioClip wrongClip; /*!< This audio clip is played when an "object" is released incorrectly at its corresponding goal position or slot */
	GameObject thisSlot; /*!< This variable represents an instantiation of the prefab "slot" */
	GameObject thisOtherSlot; /*!< This variable represents an instantiation of the prefab "otherSlot" */
	GameObject hand; /*!< This variable represents the avatar of the game */
	GameObject[] otherObjects; /*!< This array represents the other objects of the same type, i.e. all the books in the game */
	bool released; /*!< This flag is raised when the "object" is released */
	bool held; /*!< This flag is raised when the "object" has being picked up */
	bool holdable; /*!< This flag is raised when the "object" can be picked up */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	*/
	void Start () {
		difficulty = 1;
		startingScrambleRange = scrambleRange;
		goalPosition = this.transform.position;
		hand = GameObject.Find("Hand");
		released = false;
		held = false;
		holdable = false;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * slot manager, status flag, slot instantiation and destruction, position of "objects" 
	 * and GUI flag controller and other GUI components like the instructions textures
	 */
	void Update () {
		if(thisSlot){
			if(thisSlot.GetComponent<BoxCollider2D>().bounds.Intersects(this.GetComponent<BoxCollider2D>().bounds)){
				thisSlot.GetComponent<SpriteRenderer>().color = Color.green;
			}else{
				thisSlot.GetComponent<SpriteRenderer>().color = Color.white;
			}
			if (released){
				if(!thisSlot.GetComponent<BoxCollider2D>().bounds.Intersects(this.GetComponent<BoxCollider2D>().bounds)){
					this.transform.position = scrambledPosition;
					this.transform.eulerAngles = scrambledOrientation;
					PlayAudioWrong();
				}else{
					this.transform.position = goalPosition;
					Instantiate(score10,this.transform.position,Quaternion.identity);
					Camera.main.GetComponent<CameraBehaviour>().AddScore(10);
				}
				released = false;
			}
		}
		if (this.transform.position == (Vector3)goalPosition){
			this.GetComponent<SpriteRenderer>().color = Color.white;
			holdable = false;
		}else{
			holdable = true;
		}
		if(held){
			this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 2;
			if(!thisSlot){
				thisSlot = (GameObject)Instantiate(slot,goalPosition,Quaternion.identity);
				otherObjects = GameObject.FindGameObjectsWithTag("Objects");
				foreach(GameObject otherObject in otherObjects){
					if(this.gameObject != otherObject){
						if(otherObject.GetComponent<ObjectBehaviour>()){
							if(otherObject.GetComponent<ObjectBehaviour>().objectTag == this.objectTag){
								otherObject.GetComponent<ObjectBehaviour>().InstantiateOtherSlot();
							}
						}
					}
				}
			}
		} else{
			this.gameObject.GetComponent<SpriteRenderer>().sortingOrder = 1;
			if(thisSlot){
				Destroy(thisSlot);
				foreach(GameObject otherObject in otherObjects){
					if(this.gameObject != otherObject){
						if(otherObject.GetComponent<ObjectBehaviour>()){
							if(otherObject.GetComponent<ObjectBehaviour>().objectTag == this.objectTag){
								otherObject.GetComponent<ObjectBehaviour>().DestroyOtherSlot();
							}
						}
					}
				}
			}
		}
	}

	//! Scramble() function
	/*! This function puts the "objects" in a random position within a rectagle area when called. 
	 * The orientation of the "objects" is set randomly 
	 */
	void Scramble(){
		if(!holdable){
			this.transform.position = new Vector2(scrambleRange.xMin+Random.Range(0.0f,1.0f)*scrambleRange.width,scrambleRange.yMin+Random.Range(0.0f,1.0f)*scrambleRange.height);
			this.transform.eulerAngles = new Vector3(0,0,Random.Range(0,3)*90);
			scrambledOrientation = this.transform.eulerAngles;
			scrambledPosition = this.transform.position;
		}
	}

	//! Public function ChangeDifficulty()
	/*! This function increases the difficulty of the game. It's called after every "object" has being released on their
	 * corresponding goal position. After the last difficulty is successfully achieved the difficulty level starts all over again
	 */
	public void ChangeDifficulty(){
		if(difficulty == 3)
			difficulty = 1;
		else
			difficulty += 1;
		if(difficulty == 1){
			scrambleRange = startingScrambleRange;
		} else if(difficulty == 2){
			scrambleRange = new Rect(startingScrambleRange.x,startingScrambleRange.y-3f,startingScrambleRange.width,startingScrambleRange.height);
		}else{
			scrambleRange = new Rect(startingScrambleRange.x,startingScrambleRange.y-5f,startingScrambleRange.width,startingScrambleRange.height);
		}
	}

	//! Public function PlayAudioWrong()
	/*! This function plays the audio clip "wrongClip" when called and if there's another clip playing this will be stopped  
	 */
	public void PlayAudioWrong(){
		this.audio.Stop();
		this.audio.Play();
		this.audio.PlayOneShot(wrongClip);
	}

	//! Public function GrabObject()
	/*! This function makes the grabbed "object" follow the avatar "hand" until it's released. The "held" flag
	 * is raised if the "object" is "holdable"
	 */
	public void GrabObject(){
		this.transform.position = hand.transform.position + 2*(Vector3)hand.GetComponent<BoxCollider2D>().center;
		this.transform.eulerAngles = new Vector3(0,0,0);
		held = true;
	}

	//! Public function ReleaseObject()
	/*! This function makes the grabbed "object" stop following the avatar "hand" and it's released at that position. 
	 * If the "object" is released near the "thisSlot" GameObject then the "object" has reached their goal position
	 */
	public void ReleaseObject(){
		released = true;
		held = false;
	}

	//! Public function GetHoldable()
	/*! This function returns the status of the "holdable" flag
	 */
	public bool GetHoldable(){
		return holdable;
	}

	//! Public function IsInOrder()
	/*! This function returns the current status of the positions of the "object" using "holdable" and "held" flags
	 */
	public bool IsInOrder(){
		if(!held && !holdable)
			return true;
		else
			return false;
	}

	//! Public function GetThisSlot()
	/*! This function returns the GameObject "thisSlot" 
	 */
	public GameObject GetThisSlot(){
		return thisSlot;
	}

	//! Public function ThisSlotExists()
	/*! This function returns the status of the instantiation of the GameObject "thisSlot" 
	 */
	public bool ThisSlotExists(){
		return (bool)thisSlot;
	}

	//! Public function InstantiateOtherSlot()
	/*! This function instantiate the GameObject prefab "otherSlot" when called 
	 */
	public void InstantiateOtherSlot(){
		if(!thisOtherSlot)
			if(holdable)
				thisOtherSlot = (GameObject)Instantiate(otherSlot,this.goalPosition,Quaternion.identity);
	}

	//! Public function InstantiateOtherSlot()
	/*! This function destroys the instantiated GameObject "thisOtherSlot" when called 
	 */
	public void DestroyOtherSlot(){
		if(thisOtherSlot)
			Destroy(thisOtherSlot);
	}

	//* OnTriggerEnter2D() function
	/*! This function plays the "pointingClip" audio clip  when a trigger collision occurs with the "hand" GameObject  
	 */
	void OnTriggerEnter2D(Collider2D other){
		if(other.name == "Hand"){
			if(holdable && !hand.GetComponent<HandBehaviour>().IsHoldingAnObject()){
				this.audio.Stop();
				this.audio.Play();
				this.audio.PlayOneShot(pointingClip);
			}
		}
	}

}
