﻿using UnityEngine;
using System.Collections;

//! ScoreBehaviour class
/*! This class represents the behaviour of the GameObjects score after a Cube is destroyed while the score is updated
 * each score moves towards the centre of the screen and have a lifespan of 1.5 seconds
 */
public class ScoreBehaviour : MonoBehaviour {

	float timeShown; /*!< This variable represents the life time of the score GameObject */
	bool created; /*!< Boolean variable used to play the audio of this GameObject */
	public AudioClip scoreClip; /*!< This variable represent the AudioClip to be played */

	//! Start() function
	/*! This function is called on every Scene change in Unity 
	 * it initializes the value of the variables described above
	 * excepting the public variables initialized in the Unity editor
	 */
	void Start () {
		timeShown = 0.0f;
		created = true;
	}
	
	//! Update() function
	/*! This function is called every frame it refreshes the
	 * movement, lifetime and the destruction of GameObjects "Score"
	 */
	void Update () {
		if(created){
			this.audio.PlayOneShot(scoreClip);
			created = false;
		}
		this.transform.position = Vector2.MoveTowards(this.transform.position,new Vector2(0,0),0.025f);
		timeShown += Time.deltaTime;
		if(timeShown >= 1.5f){
			Destroy(this.gameObject);
		}
	}
}
