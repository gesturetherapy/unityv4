﻿using UnityEngine;
using System.Collections;

public class SpatulaControlScript : MonoBehaviour {


	Animator anim;
	private static GameObject meat;
	Collider2D[] colliders;
	int score = 0;
	//float speed = 0.1f;
	//float h = 0;
	//float v = 0;
	private static bool paused = false;
	
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!paused){
			anim.SetBool("Flip",false);
			meat = GameObject.Find("Meat(Clone)");
			if(meat){
				colliders = Physics2D.OverlapAreaAll((Vector2)this.GetComponent<BoxCollider2D>().bounds.min,(Vector2)this.GetComponent<BoxCollider2D>().bounds.max,LayerMask.GetMask("Meat"));
				foreach(Collider2D coll in colliders){
					if(coll == meat.GetComponent<EdgeCollider2D>()){
						Destroy(meat);
						score += 1;
						anim.SetBool("Flip",true);
					}
				}
			}
			//this.transform.position = new Vector3((Input.mousePosition.x - (Screen.width/2))/200,(Input.mousePosition.y - ((float)0.8*Screen.height/2))/200,Input.mousePosition.z);
			if(transform.position.x >= 4.5) transform.position = new Vector3((float)4.49,transform.position.y,transform.position.z);
			if(transform.position.x <= -4.5) transform.position = new Vector3((float)-4.49,transform.position.y,transform.position.z);
			if(transform.position.y >= 3) transform.position = new Vector3(transform.position.x,(float)2.99,transform.position.z);
			if(transform.position.y <= -1.4) transform.position = new Vector3(transform.position.x,(float)-1.39,transform.position.z);
			//h = speed * Input.GetAxis("Mouse X");
			//v = speed * Input.GetAxis("Mouse Y");
			if((transform.position.x < 4.5 && transform.position.x > -4.5) && (transform.position.y < 3 && transform.position.y > -1.4))
				transform.position = new Vector3((float)(Input.GetAxis("Horizontal")),Input.GetAxis("Vertical"));
				//transform.Translate(h, v, 0);
		}
	}

	public void Pause(bool pause){
		paused = pause;
	}

	public int getScore(){
		return score;
	}

}
